//@ts-check
const net = require('net');
var Protocol = require('../protocol/Protocol').Protocol;
var WebSocketClient = require('websocket').client;
module.exports.TetrinetClient = class TetrinetClient {
    constructor() {
        this.protocol = new Protocol();
        this.client = new WebSocketClient();
		this.connection = null;
    }

    connect({username, host = '127.0.0.1', port = 1337}) {
		this.client.connect('ws://'+host+':'+port); 
    }
	this.client.on('connect', function(connection) {
            var nick = username;
            var _version = "1.13";
            var ip = host.split('.');
            var t = false;
			this.protocol = new Protocol();
            var strConexao = this.protocol.encodeLogin(nick,_version,ip,t);
            connection.sendUTF(strConexao);	
			    /**
     * 
     * @param {string} message 
     */
    sendMessage(message) {
        const encodedMessage = this.protocol.encodeMessage(message);
        connection.sendUTF(encodedMessage);
    }
  /**
     * 
     * @param {(message: string) => void} callback 
     */
    onReceiveData(callback) {
        this.client.on('message', (data) => {
            const decodedMessage = this.protocol.decodeMessage(data.utf8Data);
            callback(decodedMessage);
        });
    }

    /**
     * 
     * @param {() => void} callback 
     */
    closeConnection(callback) {
        this.connection.end(callback)
    }
    
    /**
     * 
     * @param {(err: any) => void} callback 
     */
    onError(callback) {
        this.client.on('error', (err) => {
            callback(err);
        })
    }

    /**
     * 
     * @param {() => void} callback 
     */
    onEnd(callback) {
        this.client.on('end', () => {
            callback();
        })
    }

    /**
     * 
     * @param {(message: string) => void} message 
     */
    startGame(message) {
        const startGameMessage = message;
        console.log(startGameMessage);
        this.connection.sendUTF(startGameMessage);
        console.log('Start Game Message Sent');
        const ack = Buffer.from('ÿ', 'ascii');
        this.connection.sendUTF(ack);
    }

    sendChatMessage(message,playerNum) {
        const messageToSend = ('pline ' + playerNum + " " + message);
        //console.log(messageToSend);
        this.connection.sendUTF(messageToSend);
        const ack = Buffer.from('ÿ', 'ascii');
        this.connection.sendUTF(ack);
    }


});   

 
};

